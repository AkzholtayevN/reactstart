import React  from 'react';
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";
import './App.scss';
import { Home } from "./pages/home/Home";
import { Posts } from "./pages/posts/Posts";
import {PostItem} from "./pages/posts/PostItem";
import {Chat} from "./pages/chat/Chat";

function App() {

  return (
      <Router>
          <div className="App">
              <nav className="App-navigation">
                <ul>
                    <li><Link to="/">Home</Link></li>
                </ul>
                  <ul>
                      <li><Link to="/login">Login</Link></li>
                  </ul>
                  <ul>
                      <li><Link to="/posts">Posts</Link></li>
                  </ul>
              </nav>
              <div className="App-wrapper">
                  <Switch>
                    <Route exact path="/">
                        <Home />
                    </Route>
                      <Route exact path="/posts">
                          <Posts />
                      </Route>
                      <Route exact path="/chat">
                          <Chat />
                      </Route>
                      <Route path="/posts/:id" render={ routeProps => <PostItem id={ routeProps.match.params.id } /> } />
                      <Route exact path="**">
                          <h2>Not found</h2>
                      </Route>
                  </Switch>
              </div>
          </div>
      </Router>

  );
}

export default App;



